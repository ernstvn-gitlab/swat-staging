require "rspec"

def with_environment(args: [])
  ENV["COG_ARGC"] = args.length.times { |n| ENV["COG_ARGV_#{n}"] = args[n] }.to_s
  yield
ensure
  ENV.delete("COG_ARGC")
  args.length.times { |n| ENV.delete("COG_ARGV_#{n}") }
end
