# GitLab SWAT for staging

This project is a copy of [GitLab SWAT](https://gitlab.com/gitlab-cog/swat) rebranded for staging, just to setup a different relay

# Configuring in cog

## Environment variables

* **SCRIPTS_REMOTE_URL** url pointing to the remote repository
* **SCRIPTS_LOCAL_PATH** folder where the remote repository will be downloaded to
* **RAILS_RUNNER_COMMAND** command used to run rails, for example: rails runner ./scripts/lib/swat_run.rb
* **RAILS_WORKING_DIR** working dir in which to execute the rails runner command

## Cog Commands

* `dryrun <script> [args]` executes the given script with arguments in dryrun mode
* `strike <script> [args]` executes the given script with arguments in execute mode
* `reload [-f]` clones or pulls the scripts repo, use _-f_ to wipe the repo and clone it from scratch
